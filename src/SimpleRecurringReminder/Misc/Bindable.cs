﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SimpleRecurringReminder.Misc
{
    internal abstract class Bindable : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName]string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}