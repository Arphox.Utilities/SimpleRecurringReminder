﻿using System;
using System.Windows;
using System.Windows.Threading;

namespace SimpleRecurringReminder
{
    internal sealed class RecurringReminder
    {
        private readonly Action _finishCallback;
        private readonly DispatcherTimer _timer = new DispatcherTimer();
        private readonly string _message;

        public RecurringReminder(TimeSpan interval, string message, Action finishCallback)
        {
            _finishCallback = finishCallback;
            _message = string.IsNullOrWhiteSpace(message) ? "You asked me to remind you! :)" : message;

            _timer.Interval = interval;
            _timer.Tick += TimerTick;
        }

        public void Start() => _timer.Start();

        private void TimerTick(object sender, EventArgs e)
        {
            _timer.Stop();
            MessageBoxResult result = MessageBox.Show(_message, "Reminder", MessageBoxButton.OKCancel, MessageBoxImage.Information);
            switch (result)
            {
                case MessageBoxResult.OK:
                    _timer.Start();
                    break;
                case MessageBoxResult.Cancel:
                    _finishCallback();
                    break;
                default:
                    MessageBox.Show("Unexpected MessageBoxResult, please contact the developer!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    Environment.Exit(0);
                    break;
            }
        }
    }
}