# SimpleRecurringReminder
A simple application that can be set to repeatedly remind the user to do something.

See the file [help.txt](src/SimpleRecurringReminder/Resources/help.txt) for a brief description of the application.


In action:  
![](docs/in-action.gif)